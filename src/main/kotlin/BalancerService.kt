class BalancerService (
    val balancers: List<Balancer>
        ){
     fun getBalancedPositions(str: String): List<PositionType> {
        val charStack = ArrayDeque<Char>()
        val indexStack = ArrayDeque<Int>()
        val positions = mutableListOf<PositionType>()
        val possibleOpenings = balancers.map { it.opening }
        val possibleClosings = balancers.map { it.closing }

        str.forEachIndexed { index, c ->
            if (c in possibleOpenings) {
                charStack.addFirst(c)
                indexStack.addFirst(index)
            }
            if (charStack.isEmpty()) return listOf()

            if (c in possibleClosings) {
                val opening = charStack.removeFirst()
                balancers.find { it.closing == c }?.let {
                    if (!it.closes(opening)) return listOf()
                }
                positions.add(PositionType(indexStack.removeFirst(), index, "$opening$c"))
            }
        }
        return if (charStack.isEmpty()) positions else listOf()
    }
}