fun main(args: Array<String>) {

    val invalidString = "(((({{}})))()[][][{{}}]"
    val validString = "(()(){[]}{})"
    val balancers = listOf(
        Balancer('(', ')'),
        Balancer('[', ']'),
        Balancer('{', '}')
    )
    val balancerService = BalancerService(balancers)

    val validPositions = balancerService.getBalancedPositions(validString)
    println("String is balanced: ${validPositions.isNotEmpty()}")
    validPositions.forEach { println(it) }

    val invalidPositions = balancerService.getBalancedPositions(invalidString)
    println("String is balanced: ${invalidPositions.isNotEmpty()}")
    invalidPositions.forEach { println(it) }
}

