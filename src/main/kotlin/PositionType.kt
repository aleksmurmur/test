data class PositionType(
    val openingPosition: Int,
    val closingPosition: Int,
    val type: String
)