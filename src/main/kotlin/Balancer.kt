class Balancer(
    val opening: Char,
    val closing: Char
) {
    fun closes(c: Char) =
        (c == opening)
}